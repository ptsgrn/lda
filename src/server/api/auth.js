// (c) 2021 Patsagorn Yuenyong
// MIT

const Gun = require("gun")
let gun = Gun("http://172.20.13.176:8765/gun")

let user = gun.user()


class Auth {
    constructor(){
        // nothing
    }
    async register({username, password}){
        user.create(username, password, (ok, pub)=> {
            return pub
        })
    }
    async login({username, password}) {
        user.auth(username, password,(ok) => {
            return ok
        })
    }
    async isLogin() {
        await user.is()
    }
    async logout() {
        await user.leave()
    }
    async drop({username, password}){
        user.delete(username, password, (ok) => {
            return ok
        })
    }
    async change({username, password, newpass}){
        user.auth(username, password, (ok)=>{
            return ok
        }, { change: newpass })
    }
}

module.exports = Auth