const express = require('express');
const router = express.Router();
const Auth = require("./auth")


const Gun = require("gun")
let gun = Gun("http://172.20.13.176:8765/gun")

let user = gun.user()


function response(res,{status=200,code,message,msg,ok}){
  message = message || msg
  return res.status(status).json({code,message,ok})
}

router.get('*', (req, res) => {
  return response(res, {code:"NOGET", msg: "ไม่รองรับการเข้าถึง API ผ่าน GET"})  
})

router.post('/auth', async ({ data: { username, password, action }}, res) => {
  console.log("username:",username,"password:", password, "action:", action)
  if (!action) return response(res, {code: "NOACTION", message: "ต้องมี action" })
  if (!(username && password)) {
    return response(res, {code: "MISSING",msg: "ขาดชื่อผู้ใช้หรือรหัสผ่าน"})
  }
  if (action === "login") {
    user.auth(username, password,(ok)=>{
      if (!ok) return response(res,{code:"CANNOTLOGIN", msg: ok.err})
      if (ok && ok.err) return response(res, {code:"LOGINERROR",msg: ok.err})
      response(res, {code: "LOGGEDIN", msg: "logged-in"})
    })
  }
  if (action === "register" ){
      user.create(username, password, (ok, pub)=> {
        if (!ok) return response(res,{code: "CANNOTCREATEUSER", msg: ok.err})
        if (ok && ok.err) return response(res, {code: "CREATEALREADY", msg: ok.err})
        response(res, {code: "OK", msg: "created", pub})
    })
  }
})

router.use((req, res, next) => {
  res.status(404).json({message: 'API not found'});
})

module.exports = router;
